# heap_of_flags

## Description

Пока админы пилят базу данных флагов, вам срочно понадобился флаг следующего вида:

- Начинается с SiBears{
- Далее 3-4 заглавные латинские буквы
- Далее от 2-х до 5-ти цифр
- 7 символов среди которых нет `012345` и `ABCDEFGH`
- 3 буквы P O или T
- }

## Solution

`curl -X POST -d "veryverylongparam=SiBears{[A-Z]{3,4}[0-9]{2,5}[^0-5|A-H]{7}[TOP]{3}}" host:port`


## Flag

`SiBears{QMDG98Z6WOKKPTOP}`
