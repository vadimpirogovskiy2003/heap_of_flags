from flask import Flask
from flask import request
import re

app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1>Welcome to the flag database!</h1>'

@app.route('/flag', methods=['POST', 'GET'])
def get_flag():
    if request.method == 'POST':
        exp = request.values.get('veryverylongparam', 'Error')
        f = open('flags', 'r')
        print(exp)
        flags = f.read()
        result = re.findall(exp, flags)
        print(result)
        return f"<pre>{result}</pre>"
    return "Yeah your flag is... stop you don't entered request!"

if __name__ == '__main__':
    app.run(host="0.0.0.0")
